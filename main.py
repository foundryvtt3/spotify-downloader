#      This program is free software: you can redistribute it and/or modify
#      it under the terms of the GNU General Public License as published by
#      the Free Software Foundation, either version 3 of the License, or
#      (at your option) any later version.
#
#      This program is distributed in the hope that it will be useful,
#      but WITHOUT ANY WARRANTY; without even the implied warranty of
#      MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#      GNU General Public License for more details.
#
#      You should have received a copy of the GNU General Public License
#      along with this program.  If not, see <https://www.gnu.org/licenses/>.

import requests
import re
from pytube import YouTube
import youtubesearchpython
import unicodedata
import asyncio
import logging
from console_progressbar import ProgressBar

logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)

def get_spotify_playlist(playlist, folder_name):
    logger.info('Downloading playlist %s', playlist)
    accessTokenHeaders = {"app-platform": "WebPlayer", "Accept-Language": "en", "Cookie": 'sp_t=b7463bc6404e2c9526daa8434dce4151; sp_landing=https"%"3A"%"2F"%"2Fopen.spotify.com"%"2Fembed"%"2Fplaylist"%"2F6OZwRjEn8BtOs4WUrk3om7"%"3Freferrer"%"3Dhttps"%"253A"%"252F"%"252Fwww.redditmedia.com"%"252F; spref=https://www.redditmedia.com/; sp_landingref=https://www.redditmedia.com/; sp_dc=AQCswsDxGJMldZtMywts62UAUJdsIMLugLpnkDypRD2m46cZIaKd4MBiKe73g-OEAbs2E4xU1NvR1-7FS4Y0ev-A0kSwq9dpJfAsYSAmZb8; sp_key=621adaad-f2f4-40aa-9e77-c26a15320ac3'}
    responseAccessToken = requests.get("https://open.spotify.com/get_access_token?reason=transport&productType=web_player", headers=accessTokenHeaders)
    accessToken = responseAccessToken.json()['accessToken']
    headers = {'authorization': 'Bearer '+accessToken, 'Accept': 'application/json', 'Accept-Encoding': 'None'}
    playlistInfo = requests.get(url="https://api.spotify.com/v1/playlists/" + playlist + "?market=ES",
                                headers=headers)
    if folder_name == '':
        folder_name = slugify(playlistInfo.json()['name'])

    trackList = requests.get(url="https://api.spotify.com/v1/playlists/"+playlist+"/tracks?additional_types=track&market=ES", headers=headers)
    if trackList.status_code == 200:
        items = trackList.json()['items']
        pb = ProgressBar(total=len(items), prefix=playlistInfo.json()['name'], decimals=0, length=50, fill='#', zfill='·')
        logger.info('Found %s tracks on the playlist', items)
        for item in items:
            track = item['track']
            artists = track['artists']
            name = track['name']
            search_youtube_video(name, artists, folder_name, pb)



def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')


def search_youtube_video(name, artists, folder_name, pb):
    videosSearch = youtubesearchpython.VideosSearch(name + ' ' + artists[0]['name'], limit=1)
    try:
        yt = YouTube(videosSearch.result()['result'][0]['link'])

        yt.streams\
            .filter(only_audio=True)\
            .order_by('mime_type')\
            .desc()\
            .first()\
            .download('./downloads/'+folder_name)
    except:
        logger.error('Video not found for %s %s', name, artists[0]['name'])
    pb.next()


def main():
    file = open("./playlists")
    regex = r'\/playlist\/(.*?)(\?|\Z)'
    pattern = re.compile(r'\/playlist\/(.*?)(\?|\Z)')
    tasks = []
    for line in file:
        if line.startswith("#"):
            continue
        info = line.split()
        playlist = info.pop(0)
        if len(info) >= 1:
            folder_name = slugify('_'.join(info))
        else:
            folder_name = ''
        playlist = re.findall(regex, playlist, re.MULTILINE)[0]
        get_spotify_playlist(playlist[0], folder_name)
    file.close()

if __name__ == "__main__":
    main()
